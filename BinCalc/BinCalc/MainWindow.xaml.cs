﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BinCalc
{
   
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        int num_x, num_y, num_bin, num_dec;
        string txt_x, txt_y, txt_bin, txt_dec;
        bool flag = false;

        private void btnDv_Click(object sender, RoutedEventArgs e)
        {
            txt_x = txtX.Text;
            txt_y = txtY.Text;
            if (String.IsNullOrEmpty(txtX.Text))
            {
                MessageBox.Show("The X field for a binary number is empty. Please provide a value.");
            }
            else if (String.IsNullOrEmpty(txtY.Text))
            {
                MessageBox.Show("The Y field for a binary number is empty. Please provide a value.");
            }
            else
            {
                num_x = ConvToInt(txt_x);
                bool tmp_flag = flag;
                num_y = ConvToInt(txt_y);
                if(flag || num_y==0)
                {
                    MessageBox.Show("Invalide value for Y (only binary and non zero values).");
                    num_y = 1;
                    tmp_flag = true;
                }
                num_dec = num_x / num_y;
                txt_dec = Convert.ToString(num_dec);
                txt_bin = Convert.ToString(num_dec, 2);
                txtDec.Text = txt_dec;
                txtBin.Text = txt_bin;
                if (flag || tmp_flag)
                {
                    txtDec.Text = "";
                    txtBin.Text = "";
                }
            }
        }

        private void btnClr_Click(object sender, RoutedEventArgs e)
        {
            txtBin.Text = "";
            txtDec.Text = "";
            txtX.Text = "";
            txtY.Text = "";
        }

        private void BtnClrRes_Click(object sender, RoutedEventArgs e)
        {
            txtBin.Text = "";
            txtDec.Text = "";
        }

        private void btnMl_Click(object sender, RoutedEventArgs e)
        {
            txt_x = txtX.Text;
            txt_y = txtY.Text;
            if (String.IsNullOrEmpty(txtX.Text))
            {
                MessageBox.Show("The X field for a binary number is empty. Please provide a value.");
            }
            else if (String.IsNullOrEmpty(txtY.Text))
            {
                MessageBox.Show("The Y field for a binary number is empty. Please provide a value.");
            }
            else
            {
                num_x = ConvToInt(txt_x);
                bool tmp_flag = flag;
                num_y = ConvToInt(txt_y);
                num_dec = num_x * num_y;
                txt_dec = Convert.ToString(num_dec);
                txt_bin = Convert.ToString(num_dec, 2);
                txtDec.Text = txt_dec;
                txtBin.Text = txt_bin;
                if (flag || tmp_flag)
                {
                    txtDec.Text = "";
                    txtBin.Text = "";
                }
            }
        }

        private void btnMn_Click(object sender, RoutedEventArgs e)
        {
            txt_x = txtX.Text;
            txt_y = txtY.Text;
            if (String.IsNullOrEmpty(txtX.Text))
            {
                MessageBox.Show("The X field for a binary number is empty. Please provide a value.");
            }
            else if (String.IsNullOrEmpty(txtY.Text))
            {
                MessageBox.Show("The Y field for a binary number is empty. Please provide a value.");
            }
            else
            {
                num_x = ConvToInt(txt_x);
                bool tmp_flag = flag;
                num_y = ConvToInt(txt_y);
                num_dec = num_x - num_y;
                txt_dec = Convert.ToString(num_dec);
                txt_bin = Convert.ToString(num_dec, 2);
                txtDec.Text = txt_dec;
                txtBin.Text = txt_bin;
                if (flag || tmp_flag)
                {
                    txtDec.Text = "";
                    txtBin.Text = "";
                }
            }
        }

        private int ConvToInt(string val)
        {
            int num, lenght, pow, tmp;
            num = lenght = pow = tmp = 0;
            flag = false;
            lenght = val.Length;
            
            for (int i = lenght - 1; i > -1; i--)
            {
                if (val[i] == '1')
                {
                    tmp = (int)Math.Pow(2, pow);
                }
                else if (val[i] == '0')
                {
                    tmp = 0 * (int)Math.Pow(2, pow);
                }
                else
                {
                    MessageBox.Show("Invalide binary number. Cannot convert.");
                    flag = true;
                    break;
                }
                num += tmp;
                pow++;
            }
            return num;
        }

        private void btnPl_Click(object sender, RoutedEventArgs e)
        {
            txt_x = txtX.Text;
            txt_y = txtY.Text;
            if (String.IsNullOrEmpty(txtX.Text))
            {
                MessageBox.Show("The X field for a binary number is empty. Please provide a value.");
            }
            else if (String.IsNullOrEmpty(txtY.Text))
            {
                MessageBox.Show("The Y field for a binary number is empty. Please provide a value.");
            }
            else
            {
                num_x = ConvToInt(txt_x);
                bool tmp_flag = flag;
                num_y = ConvToInt(txt_y);
                num_dec = num_x + num_y;
                txt_dec = Convert.ToString(num_dec);
                txt_bin = Convert.ToString(num_dec, 2);
                txtDec.Text = txt_dec;
                txtBin.Text = txt_bin;
                if (flag||tmp_flag)
                {
                    txtDec.Text = "";
                    txtBin.Text = "";
                }
            }
        }

        private void bntToDec_Click(object sender, RoutedEventArgs e)
        {
            num_bin = num_dec = 0;
            txt_bin = txtBin.Text;
            if (String.IsNullOrEmpty(txtBin.Text))
            {
                MessageBox.Show("The field for a binary number is empty. Please provide a value.");
            }
            else
            {
                num_dec = ConvToInt(txt_bin);
                txt_dec = Convert.ToString(num_dec);
                txtDec.Text = txt_dec;
                if(flag)
                {
                    txtDec.Text = "";
                }
            }
        }

        private void btnToBin_Click(object sender, RoutedEventArgs e)
        {
            num_bin = num_dec = 0;
            txt_dec = txtDec.Text;
            if (String.IsNullOrEmpty(txtDec.Text))
            {
                MessageBox.Show("The field for a decimal number is empty. Please provide a value.");
            }
            else if (!int.TryParse(txtDec.Text, out num_dec))
            {
                MessageBox.Show("Please input only numbers");
            }
            else
            {
                txt_bin = Convert.ToString(num_dec, 2);
                txtBin.Text = txt_bin;
            }
        }
    }
}
